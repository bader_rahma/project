# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from test_app.forms import AccountForm
from django.contrib.auth import login, authenticate,logout
from django.http import HttpResponse
from django.contrib.auth.hashers import make_password
from test_app.export import ExportFiles
from django.http import HttpResponse,HttpResponseRedirect
from django.contrib.auth.decorators import login_required


# Create your views here.

def index(request):
    
    #getting our template 
    #rendering the template in HttpResponse
    return render(request,'index.html')

def inscription(request):    
	if request.method == "POST":
		form = AccountForm(request.POST)
		if form.is_valid():
			account = form.save(commit=False)
			account.password = make_password(form.cleaned_data['password'])
			account.save()
			#user = authenticate(username=username, password=password)
			
			return render(request, 'login.html')
            
	else:
		form = AccountForm()
		return render(request, 'signup.html', {'form': form})



	return render(request, 'signup.html', {'form': form})
def login_user(request):
	#logout(request)

	username = password = ''
	if request.POST:
		username = request.POST['username']
		password = request.POST['password']
		user = authenticate(username=username, password=password)
		#print(user)
		if user is not None:
			if user.is_active:
				login(request, user)
				return render(request, 'export.html')
		else:
			render(request, 'login.html')

	return render(request, 'login.html')
def logout_view(request):
    logout(request)
    return HttpResponseRedirect('/login')

@login_required(login_url='/login/') 
def export(request):
	return render(request, 'export.html')
	
def export_file(request):
	export= ExportFiles()
	response = HttpResponse(content_type='text/csv')
	response['Content-Disposition'] = 'attachment; filename="persons.csv"'	
	writer=export.export_methode(response)
	#response.write(xlsx_data)
	return response
	