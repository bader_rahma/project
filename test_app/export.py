from test_app import models as m
import csv
import logging
logger = logging.getLogger(__name__)
from django.db.models import Count
from django.db.models import Q

import os
from os.path import expanduser as ospath
class ExportFiles:
	def export_methode(self,response):
		writer = csv.writer(response)
		roles = m.Role.objects.all().values_list('roleName', flat=True)
		headers=list(roles)
		headers.insert(0,"")
		writer.writerow(headers)
		persons = m.Person.objects.all().values_list('personName','roleId__roleName')
		if persons is not None and roles is not None:

			for person in persons:
				content=[]
				for role_name in roles:
					if list(person)[1]==role_name:
						content.append("x")
					else:
						content.append("")
				content.insert(0,list(person)[0])
				writer.writerow(content)

			total=m.Person.objects.all().values_list('roleId__roleName').annotate(total=Count('roleId__roleName'))
			total = dict(total)
			nbr=0
			nbr_person=[]
			for compteur,role_name in enumerate(roles):
				if role_name not in total.keys():
					total[role_name]=0
				for role,count in total.items():
					if role_name==role:
						nbr_person.insert(nbr,count)
						nbr=nbr+1
			nbr_person.insert(0,"Total")
			
			writer.writerow(nbr_person)	
		return writer




