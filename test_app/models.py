# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Account(User):
	adress = models.CharField(max_length=100)
	def __str__(self):
		return self.username

class Role(models.Model):
	roleId=models.AutoField(primary_key=True)
	roleName=models.CharField(max_length=100)
	parentId = models.ForeignKey("Role",null=True)
	def __str__(self):
		return self.roleName


class Person(models.Model):
	personId = models.AutoField(primary_key=True)
	personName=models.CharField(max_length=100)
	roleId=models.ForeignKey(Role,blank=True,null=True,on_delete=models.SET_NULL)
	def __str__(self):
		return self.personName



