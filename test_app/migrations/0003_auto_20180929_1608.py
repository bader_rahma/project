# -*- coding: utf-8 -*-
# Generated by Django 1.11.13 on 2018-09-29 16:08
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('test_app', '0002_auto_20180929_1600'),
    ]

    operations = [
        migrations.RenameField(
            model_name='person',
            old_name='person_id',
            new_name='personId',
        ),
        migrations.RenameField(
            model_name='person',
            old_name='person_name',
            new_name='personName',
        ),
        migrations.RenameField(
            model_name='person',
            old_name='role_id',
            new_name='roleId',
        ),
        migrations.RenameField(
            model_name='role',
            old_name='role_id',
            new_name='roleId',
        ),
        migrations.RenameField(
            model_name='role',
            old_name='role_name',
            new_name='roleName',
        ),
    ]
